package claudio.mamani.translator;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TranslatorTest {
	private Translator tr;
	
	@Before
	public void before() {
		tr=new Translator();
	}

	@Test
	public void decodeBits2MorseTest() {
		String codTranslated;
		
		String bits1="11001100111010000";		
		codTranslated=tr.decodeBits2Morse(bits1);		
		assertEquals(". . . .",codTranslated);
		
		String bits2="111111110000111001101";		
		codTranslated=tr.decodeBits2Morse(bits2);		
		assertEquals("-  . . .",codTranslated);
		
		String bits3="000000111100011011011001111";		
		codTranslated=tr.decodeBits2Morse(bits3);		
		assertEquals(".  . . . .",codTranslated);
		
		String bits4="0001010101000111111101111110111111000101111110101000101111110000000011111110111111100010001011111110101000010100";		
		codTranslated=tr.decodeBits2Morse(bits4);		
		assertEquals(". . . .  - - -  . - . .  . -   - -  .  . - . .  . .",codTranslated);
		
	}
	

	@Test
	public void translate2Human() {
		String codTranslated;
		
		String codMorse1=". . . .  - - -  . - . .  . -   - -  .  . - . .  . .";		
		codTranslated=tr.translate2Human(codMorse1);		
		assertEquals("HOLA MELI",codTranslated);
		
		String codMorse2=". . . .  - - -  . - . .  . -   - -  . . -  - .  - . .  - - -";		
		codTranslated=tr.translate2Human(codMorse2);		
		assertEquals("HOLA MUNDO",codTranslated);
		
		String codMorse3=". . - .  - - -  . - .  - -  . -  -  - - -   - . - .  - - -  - . .  . .  - - .  - - -   - -  - - -  . - .  . . .  .";		
		codTranslated=tr.translate2Human(codMorse3);		
		assertEquals("FORMATO CODIGO MORSE",codTranslated);		
		
	}
}
