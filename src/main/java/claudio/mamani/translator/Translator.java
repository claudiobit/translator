package claudio.mamani.translator;



public class Translator {
	
	private String morse;
   
    
	public Translator() {
		
	}
	
	/*
	 * Este Metodo traduce los bits en simbolos de codigo morse
	 * bits(1) <= 5 = .
	 * bits(1) >= 6 = -
	 * bits(0) <=2 = espacio entre simbolos de la misma letra
	 * bits(0) <=6 = espacion entre letras
	 * bits(0) >=7 = espacion entre palabras
	 * */
	
	public int determinaLetraMorse(char[] bit,int i,String selectedBit) {
		int cont=0;				
		
		while(i<bit.length && String.valueOf(bit[i]).equals(selectedBit)) {
			i++;
			cont++;		
		}
		
		
		if(selectedBit.equals("1")) {
			if(cont<=5)
				this.morse+=".";
			else
				this.morse+="-";
				
		}else {
			if(cont<=2)
				this.morse+=" ";
			else if(cont<=6) 
					this.morse+="  ";
				else 
					this.morse+="   ";				
				
		}
		
		
		return i-1;
	}

	/*
	 * Este metodo devuelve un cadena con el codigo morse
	 * codificado.
	 * 
	 * */
	public String decodeBits2Morse(String incomingBits) {
		this.morse="";
		char[] bit=incomingBits.toCharArray();			
				
		for (int i = 0; i < bit.length; i++) {			
			
			switch (Integer.parseInt(String.valueOf(bit[i]))) {
			case 1:				
				i=determinaLetraMorse(bit,i, "1");	
				
				break;
			case 0:								
				i=determinaLetraMorse(bit,i, "0");
				
				break;	
			default:
				break;
			}		
			
			
		}		
	
		return this.morse.trim();
	}
	
	
	/*
	 * Este metodo traduce codigo morse a texto,
	 * separa las palabras en morse en donde encuentre tres espacios
	 * en blanco, luego realiza el mismo mecanismo anterior pero con 
	 * dos espacios en blanco para determinar las letras de una palabra
	 * y por ultimo obtiene el simbolo a traducir
	 * */

	public String translate2Human(String cod) {
		String translatedCode="";
		String[] codSplit=cod.split("   ");
		for (int i = 0; i < codSplit.length; i++) {			
			String[] letterMorse=codSplit[i].split("  ");
			for (int j = 0; j < letterMorse.length; j++) {				
			String letter=letterMorse[j].trim().replaceAll(" ","");
			translatedCode+=DataHash.tableMorseToHuman.get(letter);
			}
			
			translatedCode+=" ";			
		}				
		
		return translatedCode.trim();
	}
	
	
}
