package claudio.mamani.translator;

import java.util.Collection;
import java.util.HashMap;

public class DataHash {

	public static HashMap<String, String> tableMorseToHuman = createtableMorseToHuman();
	
    private static HashMap<String, String> createtableMorseToHuman() {
    	HashMap<String, String> tableMorseToHuman = new HashMap<String, String>();
    	tableMorseToHuman.put(".-", "A");
    	tableMorseToHuman.put("-...","B");
    	tableMorseToHuman.put("-.-.","C");
    	tableMorseToHuman.put("-..","D");
    	tableMorseToHuman.put(".","E");
    	tableMorseToHuman.put("..-.","F");
    	tableMorseToHuman.put("--.","G");
    	
    	tableMorseToHuman.put("....","H");
    	tableMorseToHuman.put("..","I");
    	tableMorseToHuman.put(".---","J");
    	tableMorseToHuman.put("-.-","K");
    	tableMorseToHuman.put(".-..","L");
    	
    	tableMorseToHuman.put("--","M");
    	tableMorseToHuman.put("-.","N");    	
    	tableMorseToHuman.put("---","O");
    	tableMorseToHuman.put(".--.", "P");
    	tableMorseToHuman.put("--.-", "Q");    	
    	tableMorseToHuman.put(".-.", "R");
    	tableMorseToHuman.put("...", "S");
    	tableMorseToHuman.put("-", "T");    	
    	tableMorseToHuman.put("..-", "U");
    	tableMorseToHuman.put("...-", "V");
    	tableMorseToHuman.put(".--", "W");
    	tableMorseToHuman.put("-..-", "X");
    	tableMorseToHuman.put("-.--", "Y");
    	tableMorseToHuman.put("--..", "Z");
    	
    	tableMorseToHuman.put("-----", "0");
    	tableMorseToHuman.put(".----", "1");
    	tableMorseToHuman.put("..---", "2");
    	tableMorseToHuman.put("...--", "3");
    	tableMorseToHuman.put("....-", "4");
    	tableMorseToHuman.put(".....", "5");
    	tableMorseToHuman.put("-....", "6");
    	tableMorseToHuman.put("--...", "7");
    	tableMorseToHuman.put("---..", "8");
    	tableMorseToHuman.put("----.", "9");
    	
    	tableMorseToHuman.put(".-.-.-", "FULLSTOP");
        return tableMorseToHuman;
    }
    

}
