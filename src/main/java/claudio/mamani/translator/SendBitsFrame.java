package claudio.mamani.translator;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import javax.swing.JTextField;


public class SendBitsFrame extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel lblBits;
	private JLabel lblMorse;
	private JLabel lblMorseResultado;
	private JLabel lblTrHumano;
	private JLabel lblTrHumanoResultado;	
	private JTextField txtField;	
	private String bits = "";	
	private JPanel panelResultados;
	private JButton btnReset;
	private final int ceroCod = 48;
	private final int unoCod = 49;
	private boolean finMensaje;

	
	
	public SendBitsFrame() {
		super("Traductor morse");
		this.setLayout(new FlowLayout());
		initVariables();	

		this.setVisible(true);
		this.setSize(700, 200);
		this.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		

	}
	
	public void initVariables() {
		this.finMensaje=false;	
		
		this.lblBits = new JLabel("Ingrese bits");
		this.lblMorse = new JLabel("Resultado Morse: ");
		this.lblMorseResultado = new JLabel();
		this.lblTrHumano = new JLabel("Resultado Texto Humano: ");
		this.lblTrHumanoResultado = new JLabel();
		this.btnReset=new JButton("Reiniciar");		
		addButtonListener();
		
				
		this.txtField = new JTextField(50);
		this.add(this.lblBits);
		this.add(this.txtField);
		addTextFieldListener();

		this.panelResultados = new JPanel();
		this.panelResultados.setLayout(new GridLayout(3, 2));		
		this.panelResultados.add(this.lblMorse);
		this.panelResultados.add(this.lblMorseResultado);

		this.panelResultados.add(this.lblTrHumano);
		this.panelResultados.add(this.lblTrHumanoResultado);
		
		this.panelResultados.add(this.btnReset);
		this.add(this.panelResultados);
		
	}
	
	
	public void addButtonListener() {
		this.btnReset.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				reiniciar();
				
			}
		});
	}
	
	
	
	public void addTextFieldListener() {
		this.txtField.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				if ((e.getKeyChar() != ceroCod) && (e.getKeyChar() != unoCod) ) {
					e.consume();					
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {

			}

			@Override
			public void keyPressed(KeyEvent e) {				
				
				if ((e.getKeyChar() != ceroCod) && (e.getKeyChar() != unoCod) ) {
					e.consume();									
				}
					
				else
					if(!finMensaje)
						if (e.getKeyCode() == ceroCod || e.getKeyCode() == unoCod) 
							decode(e.getKeyCode());
					else
						decodeToHuman();

			}
		});
	}
	
	
	
	
	public void decode(int tecla) {
		String morse = "";		
		Translator translator = new Translator();

		if (tecla == ceroCod)
			bits += "0";
		else
			bits += "1";		
			
		morse = translator.decodeBits2Morse(bits);
		if(morse.contains(". - . - . -"))
			this.finMensaje=true;
		this.lblMorseResultado.setText(morse);

	}
	
	public void decodeToHuman() {
		Translator translator = new Translator();
		String humanText= translator.translate2Human(this.lblMorseResultado.getText());
		this.txtField.setEnabled(false);
		this.lblTrHumanoResultado.setText(humanText);
	}
	
	public void reiniciar() {
		this.finMensaje=false;
		this.txtField.setEnabled(true);
		this.txtField.setText("");
		this.lblMorseResultado.setText("");
		this.lblTrHumanoResultado.setText("");
		this.bits="";
		
	}
	

}
